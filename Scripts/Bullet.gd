extends KinematicBody2D

var movement = Vector2(0,0)
var speed = 180

var direction
var sfx_played = false

func _physics_process(delta):
	if not direction:
		direction = (get_global_mouse_position() - Global.playerPosition).normalized()
	var collide = move_and_collide(speed * delta * direction * Global.timeRelativity)
	if collide:
		if "EnemyBody" in collide.collider.get_name():
			if Global.inTutorial:
				Global.timeRelativity = 0
				Input.set_custom_mouse_cursor(null)
				get_parent().get_node("WinLayer").visible = true
			else:
				Global.enemyLeft -= 1
				Global.enemyOnScreen -= 1
				get_parent().get_node("EnemyLeftLabel").text = "ENEMY LEFT: " + str(Global.enemyLeft)
				if Global.enemyLeft == 0:
					Global.timeRelativity = 0
					Input.set_custom_mouse_cursor(null)
					get_parent().get_node("WinLayer").visible = true
		if "EnemyBody" in collide.collider.get_name() or "Bullet" in collide.collider.get_name():
			collide.collider.free()
		$BulletGreen.visible = false
		speed = 0
		$ExplosionAnimation.visible = true
		$ExplosionAnimation.play()
		if not sfx_played:
			sfx_played = true
			$SFX.play()

func _on_ExplosionAnimation_animation_finished():
	get_parent().remove_child(self)

func _on_SFX_finished():
	$SFX.stop()
