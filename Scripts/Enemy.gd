extends KinematicBody2D

export var targetX = 0
export var targetY = 0

onready var Bullet = preload("res://Scenes/EnemyBullet.tscn")

func _on_Timer_timeout():
	if Global.timeRelativity == 0:
		return
	var playerPosition = Global.playerPosition
	var rotation = global_position.angle_to_point(playerPosition) + 3.14
	var bullet =  Bullet.instance()
	bullet.z_index = -1
	bullet.fromPositionX = global_position.x
	bullet.fromPositionY = global_position.y
	var direction = (playerPosition - global_position).normalized()
	bullet.global_position = global_position + direction * Vector2(50, 50)
	bullet.rotate(rotation)
	get_parent().add_child(bullet)

func _physics_process(delta):
	var speed = 120 if get_parent().get_name() == "Level4" else 80
	if abs(global_position.x - targetX) > 10:
		position += Vector2(speed * delta * (1 if global_position.x < targetX else -1) * Global.timeRelativity, 0)
	if abs(global_position.y - targetY) > 10:
		position += Vector2(0, speed * delta * (1 if global_position.y < targetY else -1) * Global.timeRelativity)
