extends KinematicBody2D

var movement = Vector2(0,0)
var speed = 180

export var fromPositionX = 0
export var fromPositionY = 0

var direction
export var id = 0

func _ready():
	id = Global.bulletId
	Global.bulletId += 1

func _physics_process(delta):
	if get_parent().get_name() == "Level4":
		speed = 230
	if not direction:
		direction = (Global.playerPosition - Vector2(fromPositionX, fromPositionY)).normalized()
	var collide = move_and_collide(speed * delta * direction * Global.timeRelativity)
	if collide:
		if "Bullet" in collide.collider.get_name():
			collide.collider.free()
		elif collide.collider.get_name() == "PlayerBody" and not Global.shootedBy.has(id):
			Global.shootedBy.append(id)
			Global.health -= 1
			get_parent().get_node("HealthBar").set_health((Global.health / 5.0) * 100)
		$BulletGray.visible = false
		speed = 0
		var explosion = $ExplosionAnimation.duplicate()
		explosion.global_position = $ExplosionAnimation.global_position
		explosion.connect("animation_finished", self, "remove", [get_parent(), explosion])
		get_parent().add_child(explosion)
		explosion.visible = true
		explosion.play()
		
		var sfx = $SFX.duplicate()
		sfx.connect("finished", self, "remove", [get_parent(), sfx])
		get_parent().add_child(sfx)
		sfx.global_position = global_position
		sfx.play()
		
		get_parent().remove_child(self)

func remove(parent, obj):
	parent.remove_child(obj)
