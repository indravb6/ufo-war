extends ColorRect

onready var Enemy = preload("res://Scenes/Enemy.tscn")

export var comeFromLeft = true
export var comeFromRight = false
export var comeFromTop = false
export var comeFromBottom = false

var enemy: KinematicBody2D
var x1: int
var x2: int
var y1: int
var y2: int
var fromList = [0, 1, 2, 3]

func _ready():
	var position = get_global_rect().position
	var size = get_global_rect().size
	x1 = position.x
	y1 = position.y
	x2 = x1 + size.x
	y2 = y1 + size.y

func _physics_process(delta):
	if not enemy or get_parent().get_name() == "Level1" or get_parent().get_name() == "Tutorial":
		return
	if abs(enemy.global_position.x - enemy.targetX) <= 10 and \
	abs(enemy.global_position.y - enemy.targetY) <= 10:
		var targetX = rand_range(x1, x2)
		var targetY = rand_range(y1, y2)
		enemy.targetX = targetX
		enemy.targetY = targetY

func _on_Timer_timeout():
	if Global.enemyLeft == Global.enemyOnScreen or (Global.inTutorial and Global.inTutorial < 4):
		return
	
	randomize()
	var canFrom = [comeFromLeft, comeFromRight, comeFromTop, comeFromBottom]
	if enemy:
		return
	fromList.shuffle()
	var x
	var y
	
	enemy =  Enemy.instance()
	enemy.z_index = -1
	var targetX = rand_range(x1, x2)
	var targetY = rand_range(y1, y2)
	enemy.targetX = targetX
	enemy.targetY = targetY
	
	var positionFrom = [
		Vector2(-50, targetY),
		Vector2(1100, targetY),
		Vector2(targetX, -50),
		Vector2(targetX, 640),
		]
	
	for from in fromList:
		if canFrom[from]:
			enemy.global_position = positionFrom[from]
			break
				
	get_parent().add_child(enemy)
	Global.enemyOnScreen += 1
