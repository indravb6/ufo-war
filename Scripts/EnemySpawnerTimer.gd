extends ColorRect

onready var Enemy = preload("res://Scenes/Enemy.tscn")

export var comeFromLeft = false
export var comeFromRight = false
export var comeFromTop = false
export var comeFromBottom = false

var enemy

func _on_Timer_timeout():
	if enemy:
		return
	var position = get_global_rect().position
	var size = get_global_rect().size
	var x1 = position.x
	var y1 = position.y
	
	enemy =  Enemy.instance()
	enemy.global_position = Vector2(x1, y1)
	get_parent().add_child(enemy)
