extends Control

func _on_RestartButton_pressed():
	Global.timeRelativity = 1
	Global.enemyLeft = 5
	Global.enemyOnScreen = 0
	Global.health = 5
	get_tree().reload_current_scene()

func _on_ExitButton_pressed():
	get_tree().quit()


func _on_LinkButton_pressed():
	Global.timeRelativity = 1
	Global.enemyLeft = 5
	Global.enemyOnScreen = 0
	Global.inTutorial = 0
	Global.health = 5
	get_tree().change_scene("res://Scenes/MainScreen.tscn")
