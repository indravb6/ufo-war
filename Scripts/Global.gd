extends Node

var playerPosition = Vector2(0, 0)
var timeRelativity = 1
var enemyLeft = 5
var enemyOnScreen = 0
var inTutorial = 0
var health = 5
var bulletId = 1
var shootedBy = []

const FILE_NAME = "user://game-data.json"

var player = {
	"Tutorial": true,
	"Level1": false,
	"Level2": false,
	"Level3": false,
	"Level4": false
}

func save():
	var file = File.new()
	file.open(FILE_NAME, File.WRITE)
	file.store_string(to_json(player))
	file.close()

func loadplayer():
	var file = File.new()
	if file.file_exists(FILE_NAME):
		file.open(FILE_NAME, File.READ)
		var data = parse_json(file.get_as_text())
		file.close()
		if typeof(data) == TYPE_DICTIONARY:
			player = data
		else:
			printerr("Corrupted data!")
	else:
		printerr("No saved data!")
