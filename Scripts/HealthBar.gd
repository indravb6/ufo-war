extends ColorRect

func set_health(health):
	$HealthIndicator.rect_size = Vector2(rect_size.x * (health / 100.0) - 6, $HealthIndicator.rect_size.y)
	if health == 0:
		Global.timeRelativity = 0
		get_parent().get_node("GameOverLayer").visible = true
