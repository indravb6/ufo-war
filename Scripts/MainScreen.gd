extends Control


func _on_NewGameButton_pressed():
	Global.timeRelativity = 1
	Global.enemyLeft = 5
	Global.enemyOnScreen = 0
	Global.inTutorial = 1
	get_tree().change_scene("res://Scenes/SelectLevel.tscn")

func _on_ExitButton_pressed():
	get_tree().quit()


func _on_MainScreen_tree_entered():
	Global.loadplayer()
	Input.set_custom_mouse_cursor(null)
