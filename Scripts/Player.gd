extends KinematicBody2D

export (int) var UpSpeed = 2
export (int) var MoveSpeed = 2
export (int) var Gravity = 2

const UP = Vector2(0,-1)
const NETRAL = Vector2(0,-1)
var velocity = Vector2()

var crosshair = load("res://Assets/crosshair.png")
var reload = load("res://Assets/reload.png")
onready var Bullet = preload("res://Scenes/Bullet.tscn")

var on_reload = false

func move():
	clean_booster()
	if Global.timeRelativity == 0:
		return
	if Input.is_action_pressed('up'):
		velocity.y = -UpSpeed * Global.timeRelativity
		$BottomBooster.visible = true
	if not (Input.is_action_pressed('right') and Input.is_action_pressed('left')):
		if Input.is_action_pressed('right'):
			velocity.x = MoveSpeed * Global.timeRelativity
			$LeftBooster.visible = true
		elif Input.is_action_pressed('left'):
			velocity.x = -MoveSpeed * Global.timeRelativity
			$RightBooster.visible = true
	if Input.is_action_pressed('up') or \
	Input.is_action_pressed('right') or \
	Input.is_action_pressed('left'):
		if not $SFX.playing:
			$SFX.play()
		if Global.inTutorial == 1:
			Global.timeRelativity = 1
			if get_parent().get_node("Timer").is_stopped():
				get_parent().get_node("Timer").start()
		
func clean_booster():
	$LeftBooster.visible = false
	$BottomBooster.visible = false
	$RightBooster.visible = false
	$SFX.stop()

func set_reloading():
	if Global.inTutorial and Global.inTutorial < 3:
		return
	$Timer.start()
	Input.set_custom_mouse_cursor(reload)
	on_reload = true
	get_parent().get_node("ReloadingLabel").text = "Reloading..."

func check_shoot():
	if Global.inTutorial and Global.inTutorial < 3:
		return
	if Global.timeRelativity == 0:
		return
	if not Input.is_action_just_pressed("shoot") or on_reload:
		return
	var mouse_position = get_viewport().get_mouse_position()
	var rotation = position.angle_to_point(mouse_position) + 3.14
	var bullet =  Bullet.instance()
	var direction = (mouse_position - position).normalized()
	bullet.global_position = position + direction * Vector2(50, 50)
	bullet.rotate(rotation)
	get_parent().add_child(bullet)
	set_reloading()
	if Global.inTutorial == 3:
		get_parent().get_node("TutorialText/BottomText").text = "Untuk dapat menembak kemabali\nHarus menunggu peluru terisi"
		if get_parent().get_node("Timer").is_stopped():
			get_parent().get_node("Timer").start()
			
func gravity(delta: float):
	velocity.y += delta * Gravity * Global.timeRelativity
	velocity.y = min(velocity.y, 500 * Global.timeRelativity)
	if velocity.x > 0:
		velocity.x -= delta * Gravity * Global.timeRelativity
	elif velocity.x < 0:
		velocity.x += delta * Gravity * Global.timeRelativity
	
	if Global.timeRelativity == 0:
		velocity.x = 0
		velocity.y = 0
	
func check_shooted(collide: KinematicCollision2D):
	if not collide:
		return
	var name = collide.collider.get_name() 
	if name == "BulletBody":
		return
		
	if Global.inTutorial:
		Global.timeRelativity = 0
		get_parent().get_node("Timer").stop()
		Input.set_custom_mouse_cursor(null)
		get_parent().get_node("GameOverLayer").visible = true
	else:
		if not "EnemyBullet" in name or not Global.shootedBy.has(collide.collider.id):
			if "EnemyBullet" in name:
				Global.shootedBy.append(collide.collider.id)
			Global.health -= 1
			if get_parent().get_node("HealthBar"):
				get_parent().get_node("HealthBar").set_health((Global.health / 5.0) * 100)
	
	if name == "Map":
		velocity.x *= -1
		velocity.y *= -1
		
func _physics_process(delta):
	move()
	gravity(delta)
	check_shooted(move_and_collide(velocity))
	Global.playerPosition = position
	check_shoot()
	update_reload_time()

func update_reload_time():
	if on_reload and Global.timeRelativity:
		var second = str(stepify($Timer.time_left, 0.1))
		if len(second) == 1:
			second += ".0"
		get_parent().get_node("ReloadingLabel").text = "Reloading (" + second + ")"
 
func _on_Border_body_entered(body):
	if body.get_name() == "PlayerBody":
		if Global.inTutorial:
			Global.timeRelativity = 0
			get_parent().get_node("Timer").stop()
			Input.set_custom_mouse_cursor(null)
			get_parent().get_node("GameOverLayer").visible = true
		else:
			get_parent().get_node("HealthBar").set_health(0)
	if "Bullet" in body.get_name():
		body.free()

func _ready():
	if Global.inTutorial:
		return
	Input.set_custom_mouse_cursor(crosshair)

func _on_Timer_timeout():
	if Global.inTutorial and Global.inTutorial < 3:
		return
	Input.set_custom_mouse_cursor(crosshair)
	on_reload = false
	get_parent().get_node("ReloadingLabel").text = ""
