extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$TutorialButton.disabled = !Global.player["Tutorial"]
	$Level1Button.disabled = !Global.player["Level1"]
	$Level2Button.disabled = !Global.player["Level2"]
	$Level3Button.disabled = !Global.player["Level3"]
	$Level4Button.disabled = !Global.player["Level4"]

func _on_TutorialButton_pressed():
	Global.inTutorial = 1
	get_tree().change_scene("res://Scenes/Tutorial.tscn")


func _on_Level1Button_pressed():
	Global.inTutorial = 0
	get_tree().change_scene("res://Scenes/Level1.tscn")


func _on_Level2Button_pressed():
	Global.inTutorial = 0
	get_tree().change_scene("res://Scenes/Level2.tscn")


func _on_Level3Button_pressed():
	Global.inTutorial = 0
	get_tree().change_scene("res://Scenes/Level3.tscn")


func _on_Level4Button_pressed():
	Global.inTutorial = 0
	get_tree().change_scene("res://Scenes/Level4.tscn")
