extends Label

var time = 60

func _on_Timer_timeout():
	time -= 1 * Global.timeRelativity
	text = str(time)
	if time == 0:
		Global.timeRelativity = 0
		get_parent().get_node("GameOverLayer").visible = true
