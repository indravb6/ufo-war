extends Node2D

var crosshair = load("res://Assets/crosshair.png")

func _ready():
	Global.inTutorial = 1
	Global.timeRelativity = 0.1

func _on_Timer_timeout():
	Global.inTutorial += 1
	if Global.inTutorial == 2:
		$TutorialText/TopText.text = ""
		$TutorialText/BottomText.text = ""
		$Timer.wait_time = 3
		$Timer.start()
	if Global.inTutorial == 3:
		$TutorialText/TopText.text = "Tekan mouse kiri untuk menembak"
		$TutorialText/BottomText.text = ""
		Input.set_custom_mouse_cursor(crosshair)
		$Timer.wait_time = 8
	if Global.inTutorial == 4:
		$TutorialText/TopText.text = "Musuh Datang! Tembak dia!\nPastikan dirimu tidak tertembak"
		$TutorialText/BottomText.text = ""

