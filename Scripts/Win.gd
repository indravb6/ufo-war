extends Control

export var nextScene = ""
const FILE_NAME = "user://game-data.json"

func _on_ExitButton_pressed():
	get_tree().quit()

func _on_NextLevelButton_pressed():
	Global.timeRelativity = 1
	Global.enemyLeft = 5
	Global.enemyOnScreen = 0
	Global.inTutorial = 0
	Global.health = 5
	get_tree().change_scene("res://Scenes/" + nextScene + ".tscn")


func _on_WinLayer_visibility_changed():
	if get_parent().get_name() == "Level4":
		$NextLevelButton.visible = false
	else:
		Global.player[nextScene] = true
		Global.save()

func _on_MainMenuButton_pressed():
	Global.timeRelativity = 1
	Global.enemyLeft = 5
	Global.enemyOnScreen = 0
	Global.inTutorial = 0
	Global.health = 5
	get_tree().change_scene("res://Scenes/MainScreen.tscn")
